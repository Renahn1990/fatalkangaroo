#include <Servo.h> 
 
Servo myservo;  // create servo object to control a servo 
                // a maximum of eight servo objects can be created 
 
int pos = 0;                // variable to store the servo position 
int posicao = 2;

const int LED             = 6;
const int SERVO_CANETA    = 17; 
const int FIM_DE_CURSO    = 41;
const int CANETA_SUSPENDE = 45;
const int CANETA_ABAIXA   = 44;

// a

#define SUSPENDER       0
#define TOQUE           1
#define DIREITA         2
#define ESQUERDA        3
#define CENTRO          4 
#define SLIDE_DIREITA   5
#define SLIDE_ESQUERDA  6

#define SERVO_POS_DIREITA 155
#define SERVO_POS_ESQUERDA 80

int done_flag = 0;

void setup() 
{ 
  myservo.attach(20);  // attaches the servo on pin 20 
  
  pinMode( CANETA_SUSPENDE, OUTPUT );
  pinMode( CANETA_ABAIXA, OUTPUT );
  pinMode( LED, OUTPUT);
  pinMode( FIM_DE_CURSO, INPUT );

  //Serial.begin(38400);
} 
 
 
void loop() 
{ 
  switch( posicao )
  {
    
    case SUSPENDER:
      if( !digitalRead( FIM_DE_CURSO ) )
      {
        digitalWrite(CANETA_ABAIXA, LOW);
        digitalWrite(CANETA_SUSPENDE, LOW);
        digitalWrite(LED, LOW);
        done_flag = 1; 
      }
      else
      {
        digitalWrite(LED, HIGH);
        digitalWrite(CANETA_ABAIXA, LOW);
        digitalWrite(CANETA_SUSPENDE, HIGH);
        delay(90);
        digitalWrite(CANETA_SUSPENDE, LOW);
        delay(200);  
      }
      break;
    
      case TOQUE:
        digitalWrite(CANETA_ABAIXA, HIGH);
        digitalWrite(CANETA_SUSPENDE, LOW);
        delay(100);
        digitalWrite(CANETA_ABAIXA, LOW);
        done_flag = 1;
        break;

     case DIREITA:
        myservo.write( SERVO_POS_DIREITA );
        done_flag = 1;
        break;

     case ESQUERDA:
        myservo.write( SERVO_POS_ESQUERDA );
        done_flag = 1;
        break;          
    }
    
    if( done_flag )
    {
        done_flag = 0;
        switch( posicao ){
            case DIREITA:
              posicao = SUSPENDER;
        
              break;

            case SUSPENDER:
              posicao = ESQUERDA;
              break;

            case ESQUERDA:
              posicao = TOQUE;
              break;

            case TOQUE:
              posicao = DIREITA;       
        }
        delay(1000);
    }
    

 //if(Serial.available()) Serial.println("Hello World");
} 

